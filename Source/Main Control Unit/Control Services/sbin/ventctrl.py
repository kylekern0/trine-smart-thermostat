#!/usr/bin/python3

# Smart Thermostat
# 
# /sbin/ventctrl.py
# Vent configuration program for the smart thermostat zones
#
# Attention:
#   This file is intended to be run by systemd via the /etc/systemd/system/ventctrl.service unit
#   Instead of running this file directly, try the following commands first:
#     systemctl enable ventctrl.service
#     systemctl start ventctrl.service
#     systemctl restart ventctrl.service
#
# Author:	kakern16@my.trine.edu
# Updated:	8 April 2019

# Maximum deviation allowed from the configured temperature:
delta = 0

from ventsql import *

# Main loop - repeatedly query the DB for differences between current and configured temperatures and update vent configuration accordingly:
while (True):
	# Only bother making any checks/changes if the system fan is running:
	#if ( check_ventctl_needed () ):
	if ( True ):
		# Get latest system mode:
		current_mode = get_system_mode ()
		# Get latest current and config temperatures:
		current_temps = get_current_temps ()
		config_temps = get_config_temps ()
		
		# Create a dictionary for storing zone / vent state pairs:
		newstates = {}
		
		# Check to see which vent state is required by each zone
		for zone, setting in config_temps.items ():
			# Don't check a zone if it is not listed in the current dict:
			if (not zone in current_temps.keys()):
				continue
			
			# If in Heat mode, check if temps are lower than the setting:
			if (current_mode == "Heat"):
				if (current_temps[zone] < (setting + delta)):
					newstates[zone] = "Open"
				else:
					newstates[zone] = "Closed"
			
			# If in Cool mode, check if temps are higher than the setting:
			if (current_mode == "Cool"):
				if (current_temps[zone] > (setting - delta)):
					newstates[zone] = "Open"
				else:
					newstates[zone] = "Closed"
		
		# Publish the latest vent requirements to the database:
		publish_vent_config (newstates)
