#!/bin/bash

# Creates a shortened version of the Apache 2 access log with only 100 lines
# This makes the GUI program faster and prevents data corruption

tail -100 /var/log/apache2/access.log > /var/log/apache2/short.log
