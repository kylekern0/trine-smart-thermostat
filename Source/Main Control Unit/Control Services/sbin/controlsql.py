#!/usr/bin/python3

# Smart Thermostat
# 
# /sbin/controlsql.py
# Functions for accessing the HW control program's MySQL tables
#
# Author:	kakern16@my.trine.edu
# Updated:	23 Jan 2019

# Include the connection class for accessing the DB:
import mysql.connector as mariadb

# Establish connection to the database:
smartthermodb = mariadb.connect (user="control", password="smartthermocontrol", database="smartthermostat", autocommit=True)
cursor = smartthermodb.cursor ()

# Get the latest system modes from the configmode table:
def get_config_modes ():
	conf = {}
	cursor.execute ("SELECT component,mode FROM configmode;")
	for component, mode in cursor:
		conf[component] = mode
	return conf

# Get the latest programmed temperatures from the configtemps table:
def get_config_temps ():
	conf = {}
	cursor.execute ("SELECT zone,temp FROM configtemps;")
	for zone, temp in cursor:
		conf[int(zone)] = int(temp)
	return conf

# Get the latest measured temperatures from the currenttemps table:
def get_current_temps ():
	temps = {}
	cursor.execute ("SELECT zone,temp FROM currenttemps;")
	for zone, temp in cursor:
		temps[int(zone)] = int(temp)
	return temps

# Publish the latest acutal system states to the currentmode table:
def publish_actual_modes (actual):
	if (type (actual) != dict): return True	# Parameter must be a dictionary of components to modes
	for component, mode in actual.items():
		cursor.execute ("UPDATE currentmode SET mode='%s' WHERE component='%s';" % (mode, component))
