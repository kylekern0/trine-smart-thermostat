#!/usr/bin/python3

# Smart Thermostat
# 
# /sbin/ventsql.py
# Functions for accessing the vent control program's MySQL tables
#
# Author:	kakern16@my.trine.edu
# Updated:	8 April 2019

# Include the connection class for accessing the DB:
import mysql.connector as mariadb

# Establish connection to the database:
smartthermodb = mariadb.connect (user="control", password="smartthermocontrol", database="smartthermostat", autocommit=True)
cursor = smartthermodb.cursor ()

# Get the latest system modes from the configmode table:
def get_system_mode ():
	conf = {}
	cursor.execute ("SELECT component,mode FROM configmode;")
	for component, mode in cursor:
		conf[component] = mode
	return conf["HVAC"]

# Check to see if the vent-operating system should be disabled:
def check_ventctl_needed ():
	conf = {}
	cursor.execute ("SELECT component,mode FROM currentmode;")
	for component, mode in cursor:
		conf[component] = mode
	
	# If the fans are running and the system is not off, we need to control the vents:
	if ( (conf["Fan"] != "Off") and (conf["HVAC"] != "Off")):
		return True
	# Otherwise we don't:
	else:
		return False

# Get the latest programmed temperatures from the configtemps table:
def get_config_temps ():
	conf = {}
	cursor.execute ("SELECT zone,temp FROM configtemps;")
	for zone, temp in cursor:
		conf[int(zone)] = int(temp)
	return conf

# Get the latest measured temperatures from the currenttemps table:
def get_current_temps ():
	temps = {}
	cursor.execute ("SELECT zone,temp FROM currenttemps;")
	for zone, temp in cursor:
		temps[int(zone)] = int(temp)
	return temps

# Publish the latest required vent states to the table:
def publish_vent_config (actual):
	if (type (actual) != dict): return True	# Parameter must be a dictionary of components to modes
	for zone, state in actual.items():
		cursor.execute ("UPDATE configvents SET state='%s' WHERE zone='%d';" % (state, zone))
