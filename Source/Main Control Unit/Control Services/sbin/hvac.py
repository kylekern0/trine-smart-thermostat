# Smart Thermostat
# 
# /sbin/hvac.py
# Controls the HVAC system's fan, air conditioner, and furnace
#
# Author:	kakern16@my.trine.edu
# Updated:	6 Feb 2019
#   Inverted On/Off pin logic to account for hardware inverter
#   Fixed a typo which caused the cool function to try to turn on the furnace instead of ac
# Updated:	16 Feb 2019
#   Renamed Appliance class to AppliancePower and added ApplianceSelect class to prepare for new relay configuration
# Updated:	1 Apr 2019
#   Changed implementation of ApplianceSelect class within HVAC to account for new relay wiring (flipped heat and cool positions)

# The default LED GPIO object can be used to control logical devices, like the relays:
from gpiozero import LED

# Needed to wait for relay to latch after driving pins:
from time import sleep

# Operates the relay for a single HVAC appliance:
class AppliancePower:
	# Constructor defines the pins needed to set or reset the control relay:
	def __init__ (this, set_pos_pin, set_neg_pin):
		this.set_pos = LED (set_pos_pin)	# Drive high to set, low to reset
		this.set_neg = LED (set_neg_pin)	# Drive low to set, high to reset
		
		# Ensure that the pins begin in the off state:
		# ('on' will become 'off' at the relay due to inverter)
		this.set_pos.on ()
		this.set_neg.on ()
		
		# Store the state of the appliance: True for on, False for off
		this.state = False
	# End __init__
	
	# Turn on the appliance by driving the relay to the set configuration:
	def off (this):
		# Drive the pins appropriately:
		# ('on' and 'off' will be flipped by the inverter)
		this.set_pos.off ()
		this.set_neg.on ()
		
		# Wait for the relay to latch:
		sleep (0.008)	# Max set time for the V23079 is 4 ms
		
		# Return the pins to the ground state:
		this.set_pos.on ()
		
		# Update the state of the appliance:
		this.state = True
	# End on
	
	# Turn off the appliance by driving the relay in the opposite configuration to switching on:
	def on (this):
		# Drive the pins appropriately:
		# ('on' and 'off' will be flipped by the inverter)
		this.set_pos.on ()
		this.set_neg.off ()
		
		# Wait for the relay to latch:
		sleep (0.008)	# Max reset time for the V23079 is 4 ms
		
		# Return the pins to the ground state:
		this.set_neg.on ()
		
		# Update the state of the appliance:
		this.state = False
	# End off
	
	# Get the current power state of the appliance:
	def get_state (this):
		return this.state
	# End get_state
# End AppliancePower

# Operates the relay for an appliance-selecting switch:
class ApplianceSelect:
	# Constructor defines the pins needed to set or reset the control relay, and the initial state of the relay:
	def __init__ (this, set_pos_pin, set_neg_pin, initial_state=False):
		this.set_pos = LED (set_pos_pin)	# Drive high to set, low to reset
		this.set_neg = LED (set_neg_pin)	# Drive low to set, high to reset
		
		# Ensure that the pins begin in the off state:
		# ('on' will become 'off' at the relay due to inverter)
		this.set_pos.on ()
		this.set_neg.on ()
		
		# Switch the relay into the provided initial position (True for set, False for reset):
		if (initial_state == True):
			this.SelectSet ()
		else:
			this.SelectReset ()
		
		# Store the state of the appliance: True for on, False for off
		this.state = initial_state
	# End __init__
	
	# Select the appliance wired to the set pin of the relay:
	def SelectSet (this):
		# Drive the pins appropriately:
		# ('on' and 'off' will be flipped by the inverter)
		this.set_pos.off ()
		this.set_neg.on ()
		
		# Wait for the relay to latch:
		sleep (0.008)	# Max set time for the V23079 is 4 ms
		
		# Return the pins to the ground state:
		this.set_pos.on ()
		
		# Update the state of the appliance:
		this.state = True
	# End on
	
	# Select the appliance wired to the reset pin of the relay:
	def SelectReset (this):
		# Drive the pins appropriately:
		# ('on' and 'off' will be flipped by the inverter)
		this.set_pos.on ()
		this.set_neg.off ()
		
		# Wait for the relay to latch:
		sleep (0.008)	# Max set time for the V23079 is 4 ms
		
		# Return the pins to the ground state:
		this.set_neg.on ()
		
		# Update the state of the appliance:
		this.state = False
	# End on
	
	# Get the current state of the appliance selection:
	def get_state (this):
		return this.state
	# End get_state
# End ApplianceSelect

# Translates heating/cooling commands into appropriate calls to HVAC appliances:
class HVAC:
	# Constructor defines the pins used to control each appliance:
	def __init__ (this, power_pos, power_neg, heat_pos_cool_neg, heat_neg_cool_pos, fan_pos, fan_neg, start_mode="Heat"):
		# Create Appliance objects to represent the three appliances
		this.power = AppliancePower (power_pos, power_neg)						# Hardware off/on switch
		this.switch = ApplianceSelect (heat_pos_cool_neg, heat_neg_cool_pos)	# Hardware heat/cool switch
		this.fan = AppliancePower (fan_pos, fan_neg)							# Fans auto/on switch
		
		# Begin with all systems off:
		this.power.off ()
		this.fan.off ()
		
		# Switch the system into the default starting mode, so that the current mode is known:
		if (start_mode == "Cool"):
			this.switch.SelectSet ()	# Assumes that cool call is wired to set pin
		else:
			this.switch.SelectReset ()	# Assumes that heat call is wired to reset pin
	# End __init__
	
	# Turn off the system:
	def off (this):
		this.power.off ()
	
	# Turn on the system:
	def on (this):
		this.power.on ()
	
	# Turn on the furnace:
	def heat (this):
		this.off ()					# De-power the selector relay
		#this.switch.SelectReset ()	# Switch from the air conditioner to furnace
		this.switch.SelectSet ()	# Chaged to set function to account for new wiring
	
	# Turn on the air conditioner:
	def cool (this):
		this.off ()					# De-power the selector relay
		#this.switch.SelectSet ()	# Switch from the furnace to air conditioner
		this.switch.SelectReset ()	# Changed to reset function to account for new wiring
	
	# Turn the fans on:
	def fan_on (this):
		this.fan.on ()
	
	# Let the hardware turn the fans on and off as needed:
	def fan_auto (this):
		this.fan.off ()
	
	# Return the current heating/cooling status of the system as a string:
	def get_hvac_status (this):
		# If the system power switch is on, then either the furnace or AC is on:
		if (this.power.get_state () == False):
			# If the system selector relay is on, then the air conditioner is powered:
			if (this.switch.get_state ()):
				#return "Cool"	# Old version - relay is wired in reverse now
				return "Heat"
			# Otherwise, the furnace is powered:
			else:
				#return "Heat"	# Old version - relay is wired in reverse now
				return "Cool"
		
		# Otherwise, the system is off:
		else:
			return "Off"
	# End get_hvac_status
	
	# Return the current fan mode as a string:
	def get_fan_mode (this):
		if (this.fan.get_state()):
			return "On"
		else:
			return "Auto"
	# End get_fan_mode
	
	# Return the current fan status as a string
	# Note: This differs from the mode returned by get_fan_mode in that the fan is on if set to auto and either the AC or furnace is on
	def get_fan_status (this):
		if (this.power.get_state ()):
			return "On"
		elif (this.fan.get_state()):
			return "On"
		else:
			return "Off"
	# End get_fan_status
# End HVAC
