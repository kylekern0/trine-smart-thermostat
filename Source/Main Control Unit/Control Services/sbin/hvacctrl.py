#!/usr/bin/python3

# Smart Thermostat
# 
# /sbin/hvacctrl.py
# Main control program for the house appliances
#
# Attention:
#   This file is intended to be run by systemd via the /etc/systemd/system/hvacctrl.service unit
#   Instead of running this file directly, try the following commands first:
#     systemctl enable hvacctrl.service
#     systemctl start hvacctrl.service
#     systemctl restart hvacctrl.service
#
# Author:	kakern16@my.trine.edu
# Updated:	16 Feb 2019
#   Updated header information to include file path on the MCU Raspbian system

from hwinterface import HWInterface			# For determining need for and controlling appliances
from controlsql import *

# Ensure that the fan and hvac components each have a record in currentmode:
for component in ("HVAC", "Fan"):
	cursor.execute ("SELECT mode FROM currentmode WHERE component='%s';" % (component))
	record_count = 0
	for mode in cursor:
		record_count += 1
	if (record_count == 0):
		cursor.execute ("INSERT INTO currentmode (component,mode) VALUES ('%s','None');" % (component))

# Create the hardware control object:
hardware = HWInterface ()
hardware.hardware_off ()

# Get initial states for the configured system modes:
config_modes = get_config_modes ()
# Now apply these modes to the hardware:
hardware.set_system_mode (config_modes["HVAC"])
hardware.set_fan_mode (config_modes["Fan"])
# TODO: Query hardware for current mode

# Get initial states for the configured zone temperatures:
config_temps = get_config_temps ()
# Now register these current temperatures with the hardware control object:
hardware.set_temps (config_temps)

# Main loop - repeatedly query the DB for changes in temperature and act accordingly:
while (True):
	# Check for a new mode configuration:
	new_config_modes = get_config_modes ()
	if (new_config_modes["HVAC"] != config_modes["HVAC"]):
		hardware.set_system_mode (new_config_modes["HVAC"])
	if (new_config_modes["Fan"] != config_modes["Fan"]):
		hardware.set_fan_mode (new_config_modes["Fan"])
	config_modes = new_config_modes
	
	# Check for new temperature settings:
	new_config_temps = get_config_temps ()
	changed_temps = {}
	for zone, temp in new_config_temps.items():
		if (config_temps[zone] != temp):
			changed_temps[zone] = temp
	if (changed_temps != {}):
		hardware.set_temps (changed_temps)
	
	# Obtain and publish the hardware's current actual status:
	actual_modes = {}
	actual_modes["HVAC"] = hardware.actual_mode
	actual_modes["Fan"] = hardware.actual_fan
	publish_actual_modes (actual_modes)
	
	# Determine if the hardware needs to turn on or off:
	current_temps = get_current_temps ()	# Read latest temps from ZCUs
	if (actual_modes["HVAC"] == "Off"):		# Check if the system is off and needs to be on:
		needed = hardware.check_if_needed (current_temps)
		if (needed): hardware.hardware_on ()
	else:
		done = hardware.check_if_done (current_temps)
		if (done): hardware.hardware_off ()
