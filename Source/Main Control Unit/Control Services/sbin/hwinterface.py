# Smart Thermostat
# 
# /sbin/hwinterface.py
# Controls the HVAC system's fan, air conditioner, and furnace
#
# Author:	kakern16@my.trine.edu
# Updated:	23 Jan 2019
#   Fixed an issue on line 128 where 'this.' was omitted prior to referencing a class member
# Updated:	16 Feb 2019
#   Rewrote API functions to use the new (16 Feb 2019) lower-level API from hvac.py

# Define the GPIO pins to be used for hardware control:
POWER_POS_SETTING = 2	# System power control - High to turn on, low to turn off
POWER_NEG_SETTING = 9	# System power control - Low to turn on, high to turn off
SELECT_POS_SETTING = 3	# Heat/Cool switch - High to set cool, low to reset to heat
SELECT_NEG_SETTING = 10	# Heat/Cool switch - Low to set cool, high to reset to heat
FAN_POS_SETTING = 4		# Fan control - High to set on, low to allow auto
FAN_NEG_SETTING = 11	# Fan control - Low to set on, high to allow auto

# Import the HVAC class for hardware control:
from hvac import HVAC

# Create the Control class to provide hardware control via software settings:
class HWInterface:
	# Constructor creates the HVAC object for hardware control and registers for storing state and config:
	def __init__ (this):
		# Create the HVAC hardware control object:
		this.system = HVAC (POWER_POS_SETTING, POWER_NEG_SETTING, SELECT_POS_SETTING, SELECT_NEG_SETTING, FAN_POS_SETTING, FAN_NEG_SETTING)
		
		# Store the mode of the system as a string:
		this.config_mode = this.system.get_hvac_status ()	# User-set mode
		this.actual_mode = this.system.get_hvac_status ()	# Current hardware status
		
		# Store the mode of the fan as a string:
		this.config_fan = this.system.get_fan_mode ()	# User-set mode
		this.actual_fan = this.system.get_fan_mode ()	# Current hardware status
		
		# Create a dictionary for storing desired zone temps, and initialize with valid values:
		# Keys are zone ID numbers, values are temperatures in Fahrenheit
		this.config_temps = {}
		this.config_temps[1] = 72
		this.config_temps[2] = 72
		this.config_temps[3] = 72
		this.config_temps[4] = 72
		this.config_temps[5] = 72
		this.config_temps[6] = 72
	# End __init__
	
	# Set new temperatures for one or more zones:
	# new_temps is a dict of zone ID to temperature in F
	def set_temps (this, new_temps):
		# Check that the input is a dictionary:
		if (type (new_temps) != dict): return True	# Exit with error if not dict
		
		tmp_temps = this.config_temps	# Temporary dictionary testing new setting compatibility
		
		# Copy new settings to the temporary dict, if possible:
		for zone, temp in new_temps.items ():
			if (zone in tmp_temps.keys()):			# Ensure that the zone exists
				try:
					tmp_temps[zone] = int (temp)	# Try to copy the value
				except ValueError:
					return True						# Return an error condition if the value can't be converted to an int
			else:
				return None							# Return a slightly different error condition if the zone doesn't exist
		
		# If none of the copy operations resulted in an error, replace the actual config dict:
		this.config_temps = tmp_temps
	# End set_temps
	
	# Set a new system mode by string:
	# new_mode is a string representing the system mode - "Heat", "Cool", or "Off"
	def set_system_mode (this, new_mode):
		# If "Heat" is passed, set to Heat mode:
		if (new_mode == "Heat"):
			this.config_mode = "Heat"							# Store Heat mode
			if (this.system.get_hvac_status () != "Heat"):		# Only change if not already heating
				this.system.off ()								# De-power before switching
				this.system.heat ()								# Switch to heat
			this.actual_mode = this.system.get_hvac_status ()	# Refresh hardware status flag
			return False										# No error
		
		# If "Cool" is passed, set to Cool mode:
		elif (new_mode == "Cool"):
			this.config_mode = "Cool"							# Store Cool mode
			if (this.system.get_hvac_status () != "Cool"):		# Only change if not already cooling
				this.system.off ()								# De-power before switching
				this.system.cool ()								# Switch to cool
			this.actual_mode = this.system.get_hvac_status ()	# Refresh hardware status flag
			return False										# No error
		
		# If "Off" is passed, set to Off mode:
		elif (new_mode == "Off"):
			this.config_mode = "Off"							# Store Off mode
			if (this.system.get_hvac_status () != "Off"):		# Only change if not already off
				this.system.off ()								# Turn off the power
			this.actual_mode = this.system.get_hvac_status ()	# Refresh hardware status flag
			return False										# No error
		
		# If none of those were provided, return an error:
		else:
			return True
	# End set_system_mode
	
	# Set a new fan mode by string:
	# new_mode is a string representing the fan mode - "On" or "Auto"
	def set_fan_mode (this, new_mode):
		# If "On" is passed, set to On:
		if (new_mode == "On"):
			this.config_fan = "On"							# Set On
			this.system.fan_on ()							# Turn on the fan
			this.actual_fan = this.system.get_fan_mode ()	# Refresh the hardware status
			return False									# No error
		
		# If "Auto" is passed, set to Auto:
		elif (new_mode == "Auto"):
			this.config_fan = "Auto"						# Set to auto
			this.system.fan_auto ()							# Turn on auto mode
			this.actual_fan = this.system.get_fan_mode ()	# Refresh the hardware status
			return False									# No error
		
		# If none of those were provided, return an error:
		else:
			return True
	# End set_fan_mode
	
	# Check to see if the system needs to run (to correct unwanted temperatures):
	# current_temps is a dict of zone ID to temperature in F
	# delta is an int representing the max allowable difference between current and set temperature in F
	def check_if_needed (this, current_temps, delta = 1):
		needed = False	# We'll switch this to True if any zone is off-target
		
		# Check each zone in the locally-stored dictionary
		for zone, setting in this.config_temps.items():
			# Don't check a zone if it is not listed in the current dict:
			if (not zone in current_temps.keys()):
				continue
			
			# If in Heat mode, check if temps are lower than the setting:
			if (this.config_mode == "Heat"):
				if (current_temps[zone] < (setting - delta)):
					needed = True
			
			# If in Cool mode, check if temps are higher than the setting:
			if (this.config_mode == "Cool"):
				if (current_temps[zone] > (setting + delta)):
					needed = True
		# End for
			
		return needed	# If this is True, then further code will need to turn on the appropriate hardware
	# End check_if_needed
	
	# Turn on the appropriate appliance based on the user-set system mode:
	def hardware_on (this):
		this.system.on ()									# Make a cooling call
		this.actual_mode = this.system.get_hvac_status ()	# Refresh the hardware status
		return False										# No error
	# End hardware_on
	
	# Check to see if the system needs to turn off (because target temperatures have been reached):
	# current_temps is a dict of zone ID to temperature in F
	def check_if_done (this, current_temps):
		return not this.check_if_needed (current_temps, 0)	# Done logic is just inverted needed logic
	# End check_if_done
	
	# Turn off the appliances when they are determined to be done:
	def hardware_off (this):
		this.system.off ()
		this.actual_mode = this.system.get_hvac_status ()
	# End hardware_off
# End Control
